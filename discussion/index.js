// Express Setup

const express = require ('express')
const app = express()
const port = 3001

// mongoose

const mongoose = require('mongoose');

mongoose.connect(`mongodb+srv://elaydumpit:admin123@zuitt-batch197.pa6lt4y.mongodb.net/s35?retryWrites=true&w=majority`,{

  useNewUrlParser: true,
  useUnifiedTopology: true
})

let db=mongoose.connection

db.on('error',console.error.bind(console, 'connection'));
db.once('open',() => console.log('Connection to MongoDB!'))


// creating schema
const taskSchema = new mongoose.Schema({
  name: String,

  status:{
    type: String,
    default: 'Pending'
  }
})

// MODELS
const Task = mongoose.model('Task',taskSchema);

// middleware
app.use(express.json())
app.use(express.urlencoded({extended: true}))


app.post('/tasks',(req,res) => {
  
  Task.findOne({name: req.body.name},(error,result) =>{

    if(error){
        return res.send(error)
    }else if (result != null && result.name == req.body.name){
      return res.send('Duplicate Task Found')
    } else{

      let newTask= new Task({
        name: req.body.name
      })

      newTask.save((error,savedTask) => {
        if(error){
          return console.error(error)
        }else{
          return res.status(201).send('New Task Created')
        }
    })
  
    }
  })
})

app.get('/tasks',(req,res) =>{
  Task.find({},(error,result) => {

    if(error){
      return res.send(error)

    }else{
      return res.status(200).json({

        tasks: result
      })
    }
  })
})

app.listen(port,() => console.log(`Server is running at port ${port}`))