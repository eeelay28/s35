// Express Setup

const express = require ('express')
const app = express()
const port = 4001

// mongoose

const mongoose = require('mongoose');

mongoose.connect(`mongodb+srv://elaydumpit:admin123@zuitt-batch197.pa6lt4y.mongodb.net/s35-activity?retryWrites=true&w=majority`,{

  useNewUrlParser: true,
  useUnifiedTopology: true
})

let db=mongoose.connection

db.on('error',console.error.bind(console, 'connection'));
db.once('open',() => console.log('Connection to MongoDB!'))

// creating schema
const userSchema = new mongoose.Schema({
  username: String,
  password:String
})

// MODELS
const User = mongoose.model('User',userSchema);

// middleware
app.use(express.json())
app.use(express.urlencoded({extended: true}))

app.post('/signup',(request,respond) => {

  User.findOne({username: request.body.name},(error,result) => {

    if(error){
      return respond.send(error)
    }else if (result != null && result.username == request.body.username && result.password== request.body.password){
      return respond.send('User Exist')
    } else{

      let newUser = new User({
        username: request.body.username,
        password: request.body.password
      })

      newUser.save((error,savedUser) =>{
        if(error){
          return console.error(error)
        }else{
          return respond.status(201).send('New User Created')
        }
      })
    }
  })
})

app.listen(port,() => console.log(`Server is running at port ${port}`))